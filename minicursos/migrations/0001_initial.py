# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DataEHora',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.DateField()),
                ('hora_inicial', models.TimeField()),
                ('hora_final', models.TimeField()),
            ],
            options={
                'ordering': ['data', 'hora_inicial'],
            },
        ),
        migrations.CreateModel(
            name='Minicurso',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=100, verbose_name='t\xedtulo')),
                ('descricao', models.TextField(null=True, verbose_name='descri\xe7\xe3o', blank=True)),
                ('pre_requisitos', models.TextField(null=True, verbose_name='pr\xe9 requisitos', blank=True)),
                ('max_vagas', models.PositiveIntegerField(default=0)),
                ('max_vagas_reserva', models.PositiveIntegerField(default=0)),
                ('num_inscritos', models.PositiveIntegerField(default=0, verbose_name='n\xfamero de inscritos', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0))])),
                ('num_inscritos_reserva', models.PositiveIntegerField(default=0, verbose_name='n\xfamero de inscritos reserva', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0))])),
            ],
            options={
                'ordering': ['titulo'],
            },
        ),
        migrations.CreateModel(
            name='MinicursoUsuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.PositiveIntegerField(default=1, choices=[(1, 'inscrito'), (2, 'lista de espera')])),
                ('minicurso', models.ForeignKey(related_name='usuarios', to='minicursos.Minicurso')),
                ('usuario', models.OneToOneField(related_name='minicursos', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Palestrante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=100)),
                ('thumbnail', models.ImageField(null=True, upload_to=b'photos/speakers', blank=True)),
                ('bio', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sala',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('local', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='minicurso',
            name='palestrante',
            field=models.ForeignKey(to='minicursos.Palestrante'),
        ),
        migrations.AddField(
            model_name='minicurso',
            name='sala',
            field=models.ForeignKey(to='minicursos.Sala'),
        ),
        migrations.AddField(
            model_name='dataehora',
            name='minicurso',
            field=models.ForeignKey(related_name='data_e_hora', to='minicursos.Minicurso'),
        ),
    ]
