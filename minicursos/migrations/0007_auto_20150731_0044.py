# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('minicursos', '0006_auto_20150713_2229'),
    ]

    operations = [
        migrations.AddField(
            model_name='minicurso',
            name='tipo',
            field=models.PositiveIntegerField(default=1, choices=[(1, 'palestra'), (2, 'minicurso'), (2, 'mesa-redonda')]),
        ),
        migrations.AlterField(
            model_name='minicurso',
            name='num_inscritos',
            field=models.PositiveIntegerField(default=0, verbose_name='n\xfamero de inscritos', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0))]),
        ),
        migrations.AlterField(
            model_name='minicurso',
            name='num_inscritos_reserva',
            field=models.PositiveIntegerField(default=0, verbose_name='n\xfamero de inscritos reserva', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(models.PositiveIntegerField(default=0))]),
        ),
    ]
