# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.db import IntegrityError, transaction
from django.db.models import Q
from django.shortcuts import render, redirect
from collections import OrderedDict
from rest_framework.decorators import api_view
from rest_framework.response import Response
from eventos.models import MaratonaProgramacao
from minicursos.models import Minicurso, MinicursoUsuario
from minicursos.serializer import MinicursoSerializer


def salvar_inscricao(request, minicurso):
    try:
        if minicurso.tem_vagas_disponiveis:
            MinicursoUsuario(usuario=request.user, minicurso=minicurso).save()
            minicurso.num_inscritos += 1
            messages.add_message(request, messages.SUCCESS, u"Inscrição realizada com sucesso.")
        else:
            MinicursoUsuario(usuario=request.user, minicurso=minicurso, tipo=MinicursoUsuario.Tipo.EmEspera).save()
            minicurso.num_inscritos_reserva += 1
            messages.add_message(request, messages.SUCCESS, u"Inscrito na lista de espera.")
        minicurso.save()
    except IntegrityError:
        messages.add_message(request, messages.ERROR, u"Você não pode se inscrever em mais de um minicurso.")


def diminuir_inscricao(minicurso, tipo):
    if tipo:
        minicurso.num_inscritos -= 1
    else:
        minicurso.num_inscritos_reserva -= 1
    minicurso.save()


@login_required
@transaction.atomic
def cancelar_inscricao(request, pk):
    minicurso_usuario = MinicursoUsuario.objects.get(usuario=request.user, minicurso=pk)
    if minicurso_usuario:
        diminuir_inscricao(minicurso_usuario.minicurso, minicurso_usuario.is_tipo_inscricao)
        minicurso_usuario.delete()
        messages.add_message(request, messages.SUCCESS, u"Inscrição cancelada.")
    else:
        messages.add_message(request, messages.INFO, u"Você não está inscrito neste minicurso.")
    return redirect(reverse_lazy("minicursos"))


@login_required
@transaction.atomic
def inscriver_usuario_no_minicurso(request, pk):
    #minicurso = Minicurso.objects.filter(
    #    Q(pk=pk),
    #    Q(num_inscritos__lt=Minicurso.objects.values('max_vagas')) |
    #    Q(num_inscritos_reserva__lt=Minicurso.objects.values('max_vagas_reserva'))).first()
    minicurso = Minicurso.objects.filter(pk=pk).first()
    if minicurso and minicurso.num_inscritos_reserva < minicurso.max_vagas_reserva:
        salvar_inscricao(request, minicurso)
    else:
        messages.add_message(request, messages.WARNING, u"Vagas esgotadas.")
    return redirect(reverse_lazy('minicursos'))


@login_required
def detail_minicurso(request, pk):
    minicurso = Minicurso.objects.filter(pk=pk).first()
    inscricao = MinicursoUsuario.objects.filter(usuario=request.user).first()
    return render(request, "detail_minicurso.html", {'minicurso': minicurso, 'inscricao': inscricao})


@login_required
def listar_minicursos(request):
    usuario_inscrito_desafio = MaratonaProgramacao.objects.filter(email=request.user.email)
    inscritos = OrderedDict.fromkeys(
        Minicurso.objects.filter(usuarios__usuario=request.user).order_by('data_e_hora', 'titulo'))
    nao_inscritos = OrderedDict.fromkeys(Minicurso.objects.exclude(usuarios__usuario=request.user).filter(tipo=Minicurso.Tipo.Minicurso).order_by('data_e_hora', 'titulo'))
    return render(request, 'list.html',
                  {'inscritos': inscritos,
                   'nao_inscritos': nao_inscritos,
                   'usuario_inscrito_maratona': usuario_inscrito_desafio})


@api_view(['GET'])
def api_minicurso(request):
    return Response(MinicursoSerializer(Minicurso.objects.all(), many=True, context={'request': request}).data)
