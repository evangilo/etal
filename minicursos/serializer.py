# -*- coding: utf-8 -*-
from rest_framework import serializers
from minicursos.models import Minicurso, DataEHora, Palestrante, Sala


class SalaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sala
        fields = ('id', 'local',)

class PalestranteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Palestrante
        fields = ('id', 'nome', 'thumbnail', 'bio')

class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataEHora
        fields = ('inicio', 'final')


class MinicursoSerializer(serializers.ModelSerializer):
    data_e_hora = DataSerializer(many=True)
    palestrante = PalestranteSerializer()
    sala = SalaSerializer()

    class Meta:
        model = Minicurso
        fields = ('id', 'titulo', 'descricao', 'pre_requisitos', 'tipo', 'sala', 'palestrante', 'data_e_hora')
        order_by = ('data_e_hora', 'titulo',)
