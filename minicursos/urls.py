# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^inscrever/(?P<pk>\d+)/$', 'minicursos.views.inscriver_usuario_no_minicurso', name='minicurso_inscricao'),
    url(r'^cancelar/(?P<pk>\d+)/$', 'minicursos.views.cancelar_inscricao', name='cancelar_inscricao'),
    url(r'^minicurso/(?P<pk>\d+)/$', 'minicursos.views.detail_minicurso', name='detail_minicurso'),
    url(r'^api/minicursos/$', 'minicursos.views.api_minicurso', name='api_minicurso'),
    url(r'^$', 'minicursos.views.listar_minicursos', name='minicursos'),
)
