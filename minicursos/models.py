# -*- coding: utf-8 -*-
import os
from itertools import groupby
from PIL import Image
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from djchoices.choices import DjangoChoices, ChoiceItem
from base.models import CustomUser
import datetime


class Palestrante(models.Model):
    nome = models.CharField(max_length=100)
    thumbnail = models.ImageField(upload_to="photos/speakers", blank=True, null=True)
    bio = models.TextField(blank=True, null=True)

    def get_thumbnail_filename(self):
        return os.path.join('media', self.thumbnail.name)

    def save(self, size=(300, 300)):
        super(Palestrante, self).save()
        if self.thumbnail:
            filename = self.get_thumbnail_filename()
            image = Image.open(filename)
            image.thumbnail(size, Image.ANTIALIAS)
            image.save(filename)

    def __unicode__(self):
        return self.nome


class Sala(models.Model):
    local = models.CharField(max_length=20)

    def __unicode__(self):
        return self.local


class Minicurso(models.Model):

    class Tipo(DjangoChoices):
        Minicurso = ChoiceItem(1, label=u"minicurso")
        Palestra = ChoiceItem(2, label=u"palestra")
        MesaRedonda = ChoiceItem(3, label=u"mesa-redonda")

    palestrante = models.ForeignKey(Palestrante)
    sala = models.ForeignKey(Sala)
    tipo = models.PositiveIntegerField(choices=Tipo.choices, default=Tipo.Minicurso)
    titulo = models.CharField(u"título", max_length=100)
    descricao = models.TextField(u"descrição", null=True, blank=True)
    pre_requisitos = models.TextField(u"pré requisitos", null=True, blank=True)
    max_vagas = models.PositiveIntegerField(default=0)
    max_vagas_reserva = models.PositiveIntegerField(default=0)
    num_inscritos = models.PositiveIntegerField(
        u"número de inscritos", default=0, validators=[MinValueValidator(0), MaxValueValidator(max_vagas)])
    num_inscritos_reserva = models.PositiveIntegerField(
        u"número de inscritos reserva", default=0, validators=[MinValueValidator(0), MaxValueValidator(max_vagas)])

    @property
    def vagas_disponiveis(self):
        return self.max_vagas - self.num_inscritos

    @property
    def tem_vagas_disponiveis(self):
        return self.num_inscritos < self.max_vagas

    @property
    def tem_vagas_reserva(self):
        return self.num_inscritos_reserva < self.max_vagas_reserva

    @property
    def datas(self):
        return self.data_e_hora.all()

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ['titulo']


class DataEHora(models.Model):
    minicurso = models.ForeignKey(Minicurso, related_name='data_e_hora')
    data = models.DateField()
    hora_inicial = models.TimeField()
    hora_final = models.TimeField()

    @property
    def inicio(self):
        return datetime.datetime.combine(self.data, self.hora_inicial)

    @property
    def final(self):
        return datetime.datetime.combine(self.data, self.hora_final)    

    class Meta:
        ordering = ['data', 'hora_inicial', 'hora_final']


class MinicursoUsuario(models.Model):
    class Tipo(DjangoChoices):
        Inscricao = ChoiceItem(1, label=u"inscrito")
        EmEspera = ChoiceItem(2, label=u"lista de espera")

    usuario = models.OneToOneField(CustomUser, related_name='minicursos')
    minicurso = models.ForeignKey(Minicurso, related_name='usuarios')
    tipo = models.PositiveIntegerField(choices=Tipo.choices, default=Tipo.Inscricao)

    @property
    def is_tipo_inscricao(self):
        return self.tipo == MinicursoUsuario.Tipo.Inscricao
