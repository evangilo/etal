# -*- coding: utf-8 -*-
from datetime import date

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect, render
from django.contrib import messages
from django.views.generic.edit import CreateView

from eventos.form import TrabalhoForm, MaratonaProgramacaoForm
from eventos.models import Inscricao, Evento, Trabalho, MaratonaProgramacao


def index(request):
    return render(request, 'home.html')


@login_required
def inscrever_evento(request, pk):
    evento = Evento.objects.get(pk=pk)
    Inscricao(evento=evento, usuario=request.user).save()
    messages.add_message(request, messages.SUCCESS, "Inscrição realizada com sucesso.")
    return redirect(reverse_lazy('minha_area'))


@login_required
def area_usuario(request):
    novos_eventos = Evento.objects.exclude(inscricoes__usuario=request.user).filter(data_final__gte=date.today())
    eventos_inscritos = Evento.objects.filter(inscricoes__usuario=request.user, data_final__gte=date.today())
    trabalhos = Trabalho.objects.all().filter(inscricao__usuario=request.user)
    return render(request, 'area_usuario.html', {
        'novos_eventos': novos_eventos,
        'eventos_inscritos': eventos_inscritos,
        'trabalhos': trabalhos})


@login_required
def submeter_trabalho(request):
    if request.method == 'POST':
        form = TrabalhoForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "Trabalho enviado com sucesso.")
            return redirect(reverse_lazy('minha_area'))
    else:
        form = TrabalhoForm(request.user)
    return render(request, 'submissao_artigo.html', {'form': form})


@login_required
def atualizar_maratona_programacao(request):
    inscricao = MaratonaProgramacao.objects.filter(email=request.user.email).first()

    if inscricao is None:
        return redirect(reverse_lazy('minicursos'))

    if request.method == 'POST':
        form = MaratonaProgramacaoForm(request.POST, instance=inscricao)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, u"Inscrição atualizada.")
            return redirect(reverse_lazy('minicursos'))
    else:
        form = MaratonaProgramacaoForm(instance=inscricao)
    return render(request, 'form.html', {'form': form})


class MaratonaProgramacaoCreate(CreateView):
    model = MaratonaProgramacao
    success_url = reverse_lazy('minicursos')
    fields = ('nome_equipe', 'integrante1', 'integrante2', 'integrante3', 'horario_treinamento',)

    def form_valid(self, form):
        form.instance.email = self.request.user.email
        return super(MaratonaProgramacaoCreate, self).form_valid(form)

