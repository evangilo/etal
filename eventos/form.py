# -*- coding: utf-8 -*-
from django import forms
from django.contrib.contenttypes import fields

from eventos.models import Trabalho, MaratonaProgramacao


class TrabalhoForm(forms.ModelForm):
    def __init__(self, usuario_atual, *args, **kwargs):
        super(TrabalhoForm, self).__init__(*args, **kwargs)
        self.fields['inscricao'].queryset = self.fields['inscricao'].queryset.filter(usuario=usuario_atual)
        self.fields['inscricao'].empty_label = None
        self.fields['tipo'].empty_label = None

    class Meta:
        model = Trabalho
        fields = ('inscricao', 'tipo', 'titulo', 'autores', 'orientadores', 'artigo',)
        labels = {
            'inscricao': u'Inscrição',
            'tipo': u'Área',
        }


class MaratonaProgramacaoForm(forms.ModelForm):
    class Meta:
        model = MaratonaProgramacao
        fields = ('nome_equipe', 'integrante1', 'integrante2', 'integrante3', 'horario_treinamento',)
