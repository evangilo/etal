# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descricao', models.CharField(max_length=55, verbose_name='descri\xe7\xe3o')),
                ('data_inicial', models.DateField(verbose_name='data inicial')),
                ('data_final', models.DateField(verbose_name='data final')),
            ],
        ),
        migrations.CreateModel(
            name='Inscricao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('evento', models.ForeignKey(related_name='inscricoes', to='eventos.Evento')),
                ('usuario', models.ForeignKey(related_name='inscricoes', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'inscri\xe7\xe3o',
                'verbose_name_plural': 'inscri\xe7\xf5es',
            },
        ),
        migrations.CreateModel(
            name='MaratonaProgramacao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome_equipe', models.CharField(unique=True, max_length=50, verbose_name='nome da equipe')),
                ('integrante1', models.CharField(max_length=255, verbose_name='nome do componente 1')),
                ('integrante2', models.CharField(max_length=255, verbose_name='nome do componente 2')),
                ('integrante3', models.CharField(max_length=255, null=True, verbose_name='nome do componente 3', blank=True)),
                ('horario_treinamento', models.PositiveIntegerField(default=1, verbose_name='hor\xe1rio do treinamento', choices=[(1, b'manh\xc3\xa3'), (2, b'tarde'), (3, b'noite')])),
            ],
            options={
                'verbose_name': 'Maratona de Programa\xe7\xe3o',
                'verbose_name_plural': 'Maratona de Programa\xe7\xe3o',
            },
        ),
        migrations.CreateModel(
            name='TipoTrabalho',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=55, verbose_name=b'\xc3\xa1rea')),
            ],
            options={
                'verbose_name': '\xe1rea',
                'verbose_name_plural': '\xe1reas',
            },
        ),
        migrations.CreateModel(
            name='Trabalho',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=100, verbose_name='t\xedtulo')),
                ('autores', models.CharField(help_text=b'Formato: nome1, CPF1; nome2, CPF2', max_length=1000)),
                ('orientadores', models.CharField(max_length=255)),
                ('artigo', models.FileField(upload_to=b'articles/%Y-%m-%d')),
                ('status', models.PositiveIntegerField(default=1, choices=[(1, 'em avalia\xe7\xe3o'), (2, 'reprovado'), (3, 'aprovado'), (4, 'aprovado com corre\xe7\xf5es')])),
                ('inscricao', models.ForeignKey(to='eventos.Inscricao')),
                ('tipo', models.ForeignKey(to='eventos.TipoTrabalho')),
            ],
            options={
                'ordering': ['status', 'titulo'],
            },
        ),
        migrations.AlterUniqueTogether(
            name='inscricao',
            unique_together=set([('evento', 'usuario')]),
        ),
    ]
