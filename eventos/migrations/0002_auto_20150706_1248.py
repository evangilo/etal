# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maratonaprogramacao',
            name='integrante1',
            field=models.CharField(max_length=255, verbose_name='lider'),
        ),
        migrations.AlterField(
            model_name='maratonaprogramacao',
            name='integrante2',
            field=models.CharField(max_length=255, verbose_name='nome do componente 1'),
        ),
        migrations.AlterField(
            model_name='maratonaprogramacao',
            name='integrante3',
            field=models.CharField(max_length=255, null=True, verbose_name='nome do componente 2', blank=True),
        ),
    ]
