# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0004_auto_20150713_2229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maratonaprogramacao',
            name='email',
            field=models.EmailField(default=b'', unique=True, max_length=254, verbose_name=b'e-mail de contato'),
        ),
    ]
