# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0003_maratonaprogramacao_email'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maratonaprogramacao',
            name='email',
            field=models.EmailField(unique=True, max_length=254, verbose_name=b'e-mail de contato'),
        ),
    ]
