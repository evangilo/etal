# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.db import models
from djchoices.choices import DjangoChoices, ChoiceItem

from base.models import CustomUser


class Evento(models.Model):
    descricao = models.CharField(u"descrição", max_length=55)
    data_inicial = models.DateField(u"data inicial")
    data_final = models.DateField(u"data final")

    def __unicode__(self):
        return self.descricao


class Inscricao(models.Model):
    evento = models.ForeignKey(Evento, related_name="inscricoes")
    usuario = models.ForeignKey(CustomUser, related_name="inscricoes")

    def __unicode__(self):
        return self.evento.descricao

    class Meta:
        verbose_name = u"inscrição"
        verbose_name_plural = u"inscrições"
        unique_together = ('evento', 'usuario',)


class TipoTrabalho(models.Model):
    tipo = models.CharField("área", max_length=55)

    def __unicode__(self):
        return self.tipo

    class Meta:
        verbose_name = u"área"
        verbose_name_plural = u"áreas"


class Trabalho(models.Model):
    class Status(DjangoChoices):
        EmAvaliando = ChoiceItem(1, label=u"em avaliação")
        Reprovado = ChoiceItem(2, label=u"reprovado")
        Aprovado = ChoiceItem(3, label=u"aprovado")
        AprovadoComCorrecoes = ChoiceItem(4, label=u"aprovado com correções")

    inscricao = models.ForeignKey(Inscricao)
    tipo = models.ForeignKey(TipoTrabalho)
    titulo = models.CharField(verbose_name=u"título", max_length=100)
    autores = models.CharField(max_length=1000, help_text="Formato: nome1, CPF1; nome2, CPF2")
    orientadores = models.CharField(max_length=255)
    artigo = models.FileField(upload_to="articles/%Y-%m-%d")
    status = models.PositiveIntegerField(choices=Status.choices, default=Status.EmAvaliando)

    @property
    def str_status(self):
        return self.get_status_display()

    @property
    def email_do_autor(self):
        return self.inscricao.usuario.email

    @property
    def nome_do_autor(self):
        return self.inscricao.usuario.get_full_name()

    def __unicode__(self):
        return u'Título: %s - Status: %s' % (self.titulo, self.str_status)

    class Meta:
        ordering = ['status', 'titulo']


class MaratonaProgramacao(models.Model):

    class Turno(DjangoChoices):
        manha = ChoiceItem(1, "manhã")
        tarde = ChoiceItem(2, "tarde")
        noite = ChoiceItem(3, "noite")

    email = models.EmailField('e-mail de contato', unique=True, default="")
    nome_equipe = models.CharField(u"nome da equipe", max_length=50, unique=True)
    integrante1 = models.CharField(u"lider", max_length=255)
    integrante2 = models.CharField(u"nome do componente 1", max_length=255)
    integrante3 = models.CharField(u"nome do componente 2", max_length=255, blank=True, null=True)
    horario_treinamento = models.PositiveIntegerField(
        u"horário do treinamento", choices=Turno.choices, default=Turno.manha)

    def __unicode__(self):
        return self.nome_equipe

    class Meta:
        verbose_name = u"Maratona de Programação"
        verbose_name_plural = u"Maratona de Programação"
