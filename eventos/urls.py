# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from eventos.views import MaratonaProgramacaoCreate

urlpatterns = patterns(
    '',
    url(r'^$', 'eventos.views.index', name='index'),
    #url(r'^inscricao_desafio/$', login_required(MaratonaProgramacaoCreate.as_view(template_name="form.html")), name='inscricao_desafio'),
    #url(r'^atualizar_inscricao$', 'eventos.views.atualizar_maratona_programacao', name='atualizar_inscricao'),
    # url(r'^incricao_evento/(?P<pk>\d+)/$', 'eventos.views.inscrever_evento', name="inscricao_evento"),
    # url(r'^minha_area/$', 'eventos.views.area_usuario', name="minha_area"),
    # url(r'^submeter_trabalho/$', 'eventos.views.submeter_trabalho', name='submeter_trabalho'),
)
