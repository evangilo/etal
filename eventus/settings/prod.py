from base import *
DEBUG = False

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'etal',
        'USER': 'etal',
        'PASSWORD': '@#AngBvWydX1IvQ!',
        'HOST': 'localhost',
        'PORT': '',
    }
}
STATIC_ROOT = '/home/etal/public_html'
MEDIA_ROOT = '/home/etal/public_html/media'
STATIC_URL = 'http://eventos.ifrn.edu.br/etal/static/'

MEDIA_URL = 'http://eventos.ifrn.edu.br/etal/media/'

SITE_ID = 1

FORCE_SCRIPT_NAME = '/etal'
USE_X_FORWARDED_HOST = True
