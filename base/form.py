# -*- coding: utf-8 -*-

from django import forms
from localflavor.br.forms import BRCPFField

from base.models import CustomUser


class CustomRegistrationForm(forms.ModelForm):
    cpf = BRCPFField(label='CPF', max_length=11)
    password = forms.CharField(label='senha', widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = ('nome', 'sobrenome', 'nome_completo', 'cpf', 'email', 'password', 'instituicao', 'curso')

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if password is None:
            raise forms.ValidationError('Senha não pode ser vazia.')
        return password

    def save(self, commit=True):
        user = super(forms.ModelForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


class CustomUserUpdateForm(forms.ModelForm):

    class Meta:
        model = CustomUser
        fields = ('nome', 'sobrenome', 'nome_completo', 'instituicao', 'curso')
