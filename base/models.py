# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models


class CustomUserManager(BaseUserManager):
    def create_user(self, email, cpf, nome, sobrenome, password=None):
        user = self.model(email=email, cpf=cpf, nome=nome, sobrenome=sobrenome, is_superuser=True)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, cpf, nome, sobrenome, password):
        return self.create_user(email, cpf, nome, sobrenome, password=password)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='e-mail', unique=True)
    nome = models.CharField(verbose_name='nome', max_length=30)
    sobrenome = models.CharField(verbose_name='sobrenome', max_length=30)
    nome_completo = models.CharField(verbose_name=u'nome completo', max_length=255, default="")
    cpf = models.CharField(verbose_name='CPF', unique=True, max_length=11)
    instituicao = models.CharField(verbose_name='instituição', blank=True, max_length=55)
    curso = models.CharField(verbose_name='curso', blank=True, max_length=55)

    _default_manager = CustomUserManager()
    objects = _default_manager
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['cpf', 'nome', 'sobrenome']

    def __unicode__(self):
        return "%s %s" %(self.nome, self.sobrenome)

    @property
    def first_name(self):
        return self.nome

    @property
    def last_name(self):
        return self.sobrenome

    @property
    def is_staff(self):
        return self.is_superuser

    def get_short_name(self):
        return self.nome

    @property
    def get_full_name(self):
        return "%s %s" %(self.nome, self.sobrenome)

    class Meta:
        verbose_name = 'usuário'
        verbose_name_plural = 'usuários'
        ordering = ('instituicao', 'curso', 'nome', 'sobrenome',)
