# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(unique=True, max_length=254, verbose_name=b'e-mail')),
                ('nome', models.CharField(max_length=30, verbose_name=b'nome')),
                ('sobrenome', models.CharField(max_length=30, verbose_name=b'sobrenome')),
                ('cpf', models.CharField(unique=True, max_length=11, verbose_name=b'CPF')),
                ('instituicao', models.CharField(max_length=55, verbose_name=b'institui\xc3\xa7\xc3\xa3o', blank=True)),
                ('curso', models.CharField(max_length=55, verbose_name=b'curso', blank=True)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'ordering': ('instituicao', 'curso', 'nome', 'sobrenome'),
                'verbose_name': 'usu\xe1rio',
                'verbose_name_plural': 'usu\xe1rios',
            },
        ),
    ]
