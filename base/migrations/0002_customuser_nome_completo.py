# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='nome_completo',
            field=models.CharField(default=b'', max_length=255, verbose_name=b'nome completo'),
        ),
    ]
