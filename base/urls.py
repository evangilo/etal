# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.views import login, logout

from base import views

urlpatterns = [
    url(r'^registrar/$', views.UserCreateView.as_view(), name='registrar'),
    url(r'^entrar/$', login, name='entrar'),    
    url(r'^sair/$', logout, {'next_page': 'index'}, name='sair'),
    url(r'^atualizar_usuario/$', 'base.views.atualizar_usuario', name='atualizar_usuario'),
    url(r'^usuarios/$', 'base.views.usuarios_to_csv', name='usuarios'),
]
