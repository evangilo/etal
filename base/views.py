# -*- coding: utf-8 -*-
import csv
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect, render
from django.contrib import messages

from base.form import CustomRegistrationForm, CustomUserUpdateForm
from base.models import CustomUser

from django.http import HttpResponse


class UserCreateView(CreateView):
    model = CustomUser
    form_class = CustomRegistrationForm
    success_url = reverse_lazy("entrar")
    template_name = "registration/form.html"


@login_required
def atualizar_usuario(request):
    if request.method == 'POST':
        form = CustomUserUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "Dados Atualizados.")
            return redirect(reverse_lazy('minicursos'))
    else:
        form = CustomUserUpdateForm(instance=request.user)

    return render(request, 'form.html', {'form': form})


def usuarios_to_csv(request):
    response = HttpResponse()
    response['Content-Disposition'] = 'attachment; filename="usuarios.csv"'

    writer = csv.writer(response)
    for usuario in CustomUser.objects.all():
        writer.writerow([usuario.cpf, usuario.get_full_name.encode('utf-8')])
    return response
